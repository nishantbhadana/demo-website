import Cookies from "js-cookie";
import {
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILED,
  LOGIN_USER,
  REGISTER_USER,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_FAILED
} from "./actionType";

export function loginSuccess(payload) {
  return {
    type: LOGIN_USER_SUCCESS,
    payload
  };
}

export function loginFailed(payload) {
  return {
    type: LOGIN_USER_FAILED,
    payload
  };
}

export function loggingInUser() {
  return {
    type: LOGIN_USER
  };
}

export function registerUser() {
  return {
    type: REGISTER_USER
  };
}

export function registerUserSuccess(payload) {
  return {
    type: REGISTER_USER_SUCCESS,
    payload
  };
}

export function registerUserFailed(payload) {
  return {
    type: REGISTER_USER_FAILED,
    payload
  };
}

export const loginUser = userCredentials => {
  return (dispatch, getState, api) => {
    dispatch(loggingInUser());
    api
      .post(`/auth/login`, userCredentials)
      .then(res => {
        if (res.data.success === true) {
          Cookies.set("token", res.data.jwtAccessToken);
          Cookies.set("_id", res.data.data._id);
          dispatch(loginSuccess(res.data));
        } else {
          dispatch(loginFailed(res.data));
        }
      })
      .catch(err => {});
  };
};

export const registeringUser = userCredentials => {
  return (dispatch, getState, api) => {
    dispatch(registerUser());
    api
      .post(`/web/register`, userCredentials)
      .then(res => {
        if (res.data.success === true) {
          Cookies.set("_id", res.data.data._id);
          dispatch(registerUserSuccess(res.data));
          alert(res.data.message);
        } else {
          dispatch(registerUserFailed(res.data));
        }
      })
      .catch(err => {});
  };
};
