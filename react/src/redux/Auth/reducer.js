import {
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILED,
  LOGIN_USER,
  REGISTER_USER,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_FAILED
} from "./actionType";

const initialState = {
  profile: null,
  token: null,
  loginFail: null,
  registerUser: null
};

export default function Auth(state = initialState, action) {
  switch (action.type) {
    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        profile: action.payload.data,
        token: action.payload.jwtAccessToken
      };

    case LOGIN_USER_FAILED:
      return {
        ...state,
        loginFail: state.payload
      };
    case LOGIN_USER:
      return {
        ...state,
        profile: null
      };
    case REGISTER_USER:
      return {
        ...state,
        registerUser: null
      };
    case REGISTER_USER_SUCCESS:
      return {
        ...state,
        registerUser: action.payload
      };
    case REGISTER_USER_FAILED:
      return {
        ...state,
        registerUser: action.payload
      };
    default:
      return state;
  }
}
