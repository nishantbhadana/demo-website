export const LOGIN_USER = "myApp/auth/LOGIN_USER";
export const LOGIN_USER_SUCCESS = "myApp/auth/LOGIN_USER_SUCCESS";
export const LOGIN_USER_FAILED = "myApp/auth/LOGIN_USER_FAILED";
export const REGISTER_USER = "myApp/auth/REGISTER_USER";
export const REGISTER_USER_SUCCESS = "myApp/auth/REGISTER_USER_SUCCESS";
export const REGISTER_USER_FAILED = "myApp/auth/REGISTER_USER_FAILED";
