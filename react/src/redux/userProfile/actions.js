import {
  FETCH_USER_PROFILE,
  FETCH_USER_PROFILE_SUCCESS,
  FETCH_USER_PROFILE_FAILED
} from "./actionType";

function fetchingUserProfile() {
  return {
    type: FETCH_USER_PROFILE
  };
}

function fetchingUserProfileSuccess(payload) {
  return {
    type: FETCH_USER_PROFILE_SUCCESS,
    payload
  };
}

function fetchingUserProfileFailed(payload) {
  return {
    type: FETCH_USER_PROFILE_FAILED,
    payload
  };
}

export const fetchUserProfile = userId => {
  return (dispatch, getState, api) => {
    dispatch(fetchingUserProfile());
    api
      .get(`/web/getUserProfile`, { headers: { _id: userId } })
      .then(res => {
        if (res.data.success === true) {
          dispatch(fetchingUserProfileSuccess(res.data.data));
        } else {
          dispatch(fetchingUserProfileFailed(res.data));
        }
      })
      .catch(err => {});
  };
};
