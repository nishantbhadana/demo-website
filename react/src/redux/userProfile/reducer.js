import {
  FETCH_USER_PROFILE,
  FETCH_USER_PROFILE_SUCCESS,
  FETCH_USER_PROFILE_FAILED
} from "./actionType";

const initialState = {
  UserProfile: null,
  fetchingProfile: false
};

export default function Profile(state = initialState, action) {
  switch (action.type) {
    case FETCH_USER_PROFILE:
      return {
        ...state,
        fetchingProfile: true
      };

    case FETCH_USER_PROFILE_SUCCESS:
      return {
        ...state,
        fetchingProfile: false,
        UserProfile: action.payload
      };
    case FETCH_USER_PROFILE_FAILED:
      return {
        ...state,
        fetchingProfile: false
      };
    default:
      return state;
  }
}
