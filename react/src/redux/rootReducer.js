import { combineReducers } from "redux";
import Auth from "./Auth/reducer";
import Profile from "./userProfile/reducer";

export default combineReducers({
  Auth,
  Profile
});
