import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Technologies } from "../../utils/sample.json";
import { registeringUser } from "../../redux/Auth/actions";

class registration extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fname: "",
      lname: "",
      email: "",
      phoneNumber: "",
      password: "",
      cpassword: "",
      role: "student",
      interests: [],
      showSecond: false
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.selectInterest = this.selectInterest.bind(this);
    this.removeInterest = this.removeInterest.bind(this);
    this.onSubmit1 = this.onSubmit1.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.registerUser !== null &&
      nextProps.registerUser !== this.props.registerUser
    ) {
      this.props.history.push("/profile");
    }
  }
  selectInterest(item) {
    let state = this.state;
    state.interests = [...state.interests, item];
    this.setState({ state });
  }

  removeInterest(index) {
    let state = this.state;
    state.interests = [
      ...state.interests.splice(0, index),
      ...state.interests.splice(index + 1)
    ];
    this.setState({ state });
  }

  onSubmit1(e) {
    e.preventDefault();
    this.setState({ ...this.setState, showSecond: true });
  }
  onSubmit(e) {
    e.preventDefault();
    if (this.state.password === this.state.cpassword) {
      const userData = {
        name: `${this.state.fname} ${this.state.lname}`,
        phone: this.state.phoneNumber,
        email: this.state.email,
        userType: this.state.role,
        interests: this.state.interests,
        password: this.state.password
      };
      this.props.onRegisteringUser(userData);
    } else {
      alert("password doesn't match");
    }
  }

  render() {
    return (
      <div className="main">
        {this.state.showSecond === false ? (
          <div
            className="container col-lg-6 col-sm-12"
            style={{
              padding: "2%",
              opacity: this.state.showSecond === false ? "1" : "0.8"
            }}
          >
            <form onSubmit={this.onSubmit1}>
              <h2>Registration Form</h2>
              <div className="form-group-1">
                <input
                  type="text"
                  name="fname"
                  id="fname"
                  placeholder="First Name"
                  value={this.state.fname}
                  onChange={e => this.setState({ fname: e.target.value })}
                  required
                />
                <input
                  type="text"
                  name="lname"
                  id="lname"
                  placeholder="Last Name"
                  value={this.state.lname}
                  onChange={e => this.setState({ lname: e.target.value })}
                  required
                />
                <input
                  type="email"
                  name="email"
                  id="email"
                  placeholder="Email"
                  value={this.state.email}
                  onChange={e => this.setState({ email: e.target.value })}
                  required
                />
                <input
                  type="number"
                  name="phone_number"
                  id="phone_number"
                  placeholder="Phone number"
                  pattern="/(7|8|9)\d{9}/"
                  value={this.state.phoneNumber}
                  onChange={e => this.setState({ phoneNumber: e.target.value })}
                  required
                />
                <input
                  type="password"
                  name="password"
                  id="password"
                  placeholder="Password"
                  value={this.state.password}
                  onChange={e => this.setState({ password: e.target.value })}
                  required
                />
                <input
                  type="password"
                  name="confirmPassword"
                  id="confirmPassword"
                  placeholder="Confirm Password"
                  value={this.state.cpassword}
                  onChange={e => this.setState({ cpassword: e.target.value })}
                  required
                />
                <div className="select-list">
                  <select
                    name="course_type"
                    id="course_type"
                    value={this.state.role}
                    onChange={e => this.setState({ role: e.target.value })}
                  >
                    <option value="student">Student</option>
                    <option value="mentor">Mentor</option>
                  </select>
                </div>
              </div>
              <div className="row col-12 text-center">
                <button
                  type="submit"
                  className="btn btn-info"
                  style={{ padding: "10px" }}
                >
                  Next
                </button>
              </div>
            </form>
          </div>
        ) : (
          <div className="col-lg-6 col-sm-12">
            <div
              className="container col-12"
              style={{
                overflowY: "auto",
                maxHeight: "400px",
                overflowX: "hidden",
                marginBottom: "25px",
                padding: "15px"
              }}
            >
              <div className="row col-12 text-center">
                <h2>Select Your Interests</h2>
              </div>
              <div className="row col-12">
                {Technologies.map((item, index) => (
                  <button
                    type="button"
                    className="btn btn-primary"
                    key={index}
                    onClick={() => this.selectInterest(item)}
                  >
                    {item}
                  </button>
                ))}
              </div>
            </div>
            <div className="container col-12" style={{ padding: "15px" }}>
              {" "}
              <form onSubmit={this.onSubmit}>
                <div className="row col-12 text-center">
                  <h2 style={{ marginBottom: "0px" }}>Selected Interests</h2>
                </div>
                <div
                  className="row col-12"
                  style={{
                    overflowY: "auto",
                    minHeight: "40px",
                    maxHeight: "120px",
                    overflowX: "hidden",
                    border: "2px solid #ccc",
                    margin: "10px",
                    paddingBottom: "5px"
                  }}
                >
                  {this.state.interests.map((item, index) => (
                    <button
                      type="button"
                      className="btn btn-warning"
                      key={index}
                      onClick={() => this.removeInterest(index)}
                    >
                      {`${item}  `} <span>x</span>
                    </button>
                  ))}
                </div>
                <div className="row col-12 text-center">
                  <button
                    type="submit"
                    className="btn btn-success"
                    style={{ padding: "10px" }}
                  >
                    Register
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}
      </div>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return {
    onRegisteringUser: payload => {
      dispatch(registeringUser(payload));
    }
  };
}

function mapStateToProps(state) {
  return {
    registerUser: state.Auth.registerUser
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(registration)
);
