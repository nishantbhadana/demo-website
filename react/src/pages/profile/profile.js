import React, { Component } from "react";
import { connect } from "react-redux";
import Cookies from "js-cookie";

import { fetchUserProfile } from "../../redux/userProfile/actions";

import "../profile/profile.css";

class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentWillMount = () => {
    const userId = Cookies.get("_id");
    this.props.fetchUserProfile(userId);
  };

  render() {
    let { UserProfile, fetchingProfile } = this.props;
    return (
      <div>
        {UserProfile !== null && fetchingProfile !== true ? (
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div
              className="panel panel-primary panelemail"
              style={{ margin: "25px", marginTop: "80px" }}
            >
              <div className="panel-body" style={{ color: "black" }}>
                <div className="row">
                  <div className="col-lg-4 col-md-4 col-sm-12">
                    <div className="profile-img">
                      <img src="/assets/images/profile.png" alt="" />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-4 col-md-4 col-sm-12 text-center">
                    <div className="profile-head">
                      <h5>{UserProfile.name}</h5>
                      <h6>{`User Type :   ${UserProfile.userType}`}</h6>
                      <h6>{`Email:        ${UserProfile.email}`}</h6>
                      <h6>{`Contact:      ${UserProfile.phone}`}</h6>
                    </div>
                  </div>
                  <div className="col-lg-8 col-md-8 col-sm-12">
                    <div
                      style={{
                        color: "black"
                      }}
                    >
                      <p
                        style={{
                          blockSize: "25px",
                          fontweight: "700",
                          fontSize: "x-large",
                          color: "darkslateblue"
                        }}
                      >
                        Interest
                      </p>
                      {UserProfile.interests.map((interest, index) => (
                        <label
                          style={{ padding: "25px", fontweight: "100" }}
                          key={index}
                        >
                          {interest}
                        </label>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        <div>
          {UserProfile === null && fetchingProfile === true ? (
            <div>
              {" "}
              <div className="row">
                <div className="col-md-4">
                  <div className="profile-img">
                    <img src="/assets/images/profile.png" alt="" />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="profile-head">
                    <h5>Welcome</h5>
                    <h6>Please Wait While we load Your Profile</h6>
                  </div>
                </div>
              </div>
            </div>
          ) : UserProfile !== null ? (
            <div>
              {UserProfile.userType == "student" ? (
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div className="panel panel-primary panelemail">
                    <div className="panel-heading">
                      <div>
                        <div>
                          <span> Mentors Matching Your Interest</span>
                        </div>
                      </div>
                    </div>
                    <div className="panel-body">
                      <div>
                        <table
                          class="table"
                          style={{ padding: "15px", color: "black" }}
                        >
                          <thead class="thead-dark">
                            <tr>
                              <th scope="col">Sr.</th>
                              <th scope="col">Name</th>
                              <th scope="col">Email</th>
                              <th scope="col">Interests</th>
                            </tr>
                          </thead>
                          <tbody>
                            {UserProfile.mentors.map((mentor, index) => (
                              <tr>
                                <td>{index + 1}</td>
                                <td>{mentor.name}</td>
                                <td>{mentor.email}</td>
                                <td>
                                  {mentor.interests.map((mi, mindex) => (
                                    <button
                                      className="btn btn-info"
                                      key={mindex}
                                    >
                                      {mi}
                                    </button>
                                  ))}
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              ) : null}
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchUserProfile: payload => {
      dispatch(fetchUserProfile(payload));
    }
  };
}

function mapStateToProps(state) {
  return {
    UserProfile: state.Profile.UserProfile,
    fetchingProfile: state.Profile.fetchingProfile
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
