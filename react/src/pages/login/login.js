import React, { Component } from "react";
import { connect } from "react-redux";
import { loginUser } from "../../redux/Auth/actions";

import "./login.css";

class login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      password: null,
      email: null
    };
    this.toSignUp = this.toSignUp.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.profile !== null &&
      nextProps.profile !== this.props.profile
    ) {
      this.props.history.push("/profile");
    }
  }
  toSignUp() {
    this.props.history.push("/register");
  }
  onSubmit(e) {
    e.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.onLoginUser(userData);
  }
  render() {
    return (
      <div class="main">
        <section class="sign-in">
          <div class="container" style={{ width: "30%" }}>
            <div class="signin-form text-center" style={{ padding: "20px" }}>
              <h2 class="form-title" style={{ marginBottom: "0px" }}>
                Log In
              </h2>
              <form
                onSubmit={this.onSubmit}
                class="register-form"
                id="login-form"
              >
                <div class="form-group">
                  <label for="email">
                    <i class="zmdi zmdi-account material-icons-name" />
                  </label>
                  <input
                    type="email"
                    name="email"
                    id="email"
                    placeholder="Email"
                    value={this.state.email}
                    onChange={e => this.setState({ email: e.target.value })}
                    required
                  />
                </div>
                <div class="form-group">
                  <label for="your_pass">
                    <i class="zmdi zmdi-lock" />
                  </label>
                  <input
                    type="password"
                    name="your_pass"
                    id="your_pass"
                    placeholder="Password"
                    value={this.state.password}
                    onChange={e => this.setState({ password: e.target.value })}
                    required
                  />
                </div>
                <div className="row col-12">
                  <button className="btn btn-primary" type="submit">
                    Log In{" "}
                  </button>
                  <button
                    className="btn btn-info"
                    onClick={() => this.toSignUp()}
                  >
                    Register
                  </button>
                </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return {
    onLoginUser: payload => {
      dispatch(loginUser(payload));
    }
  };
}
function mapStateToProps(state) {
  return {
    profile: state.Auth.profile
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(login);
