import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import profile from './pages/profile/profile';
import Login from './pages/login/login';
import Registration from './pages/registration/registration';
import * as serviceWorker from './serviceWorker';
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import store from "./store";

// ReactDOM.render(<Registration />, document.getElementById('root'));

ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" name="Login Page" component={Login} />
          <Route exact path="/register" name="Register Page" component={Registration} />
          {/* <Route exact path="/404" name="Page 404" component={Page404}/>
          <Route exact path="/500" name="Page 500" component={Page500}/> */}
          <Route path="/profile" name="Home" component={profile} />
        </Switch>
      </BrowserRouter>
    </Provider>,
    document.getElementById("root")
  );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
